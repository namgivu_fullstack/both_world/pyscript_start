docstring='
new_app_d=/output/path/ ./create_new_reactjs_app.sh
'
SH=`cd $(dirname ${BASH_SOURCE:-$0}) && pwd`
template_app_d="$SH/template_reactjs_app/"

[[ -z $new_app_d ]] && (echo 'Envvar new_app_d is required'; kill $$) ; mkdir -p $new_app_d

rsync -chaz "$template_app_d/" "$new_app_d/"
