--- get started
gg.  pyscript ~tutor
ref. https://pyscript.net/

> PyScript is just HTML, only a bit (okay, maybe a lot) more powerful, thanks to the rich and accessible ecosystem of Python libraries.

> Python in the browser
> Python ecosystem: Run many popular packages of Python and the scientific stack (such as numpy, pandas, scikit-learn, and more)
> Environment management: Allow users to define what packages and files to include for the page code to run


--- TODO @ pyscript
00 demo python code w/ extra/non-builtin pip package(s)
01 demo w/ reactnative app beside reactjs app
