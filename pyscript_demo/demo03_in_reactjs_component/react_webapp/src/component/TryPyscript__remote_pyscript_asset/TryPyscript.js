import { useEffect } from 'react'


/*
instead of import jscss from index.html
    <link rel="stylesheet" href="https://pyscript.net/latest/pyscript.css" />
    <script defer src="https://pyscript.net/latest/pyscript.js"></script>
we import here so as to have code wrapped inside its component

---

CAUTION we must NOT doing this way, cause pyscript required to be load once; otherwise it will have error > the name "py-script" has already been used with this registry
*/


function TryPyscript() {
    useEffect(() => {
        //region import js
        let s   = document.createElement('script')
        s.src   = 'https://pyscript.net/latest/pyscript.js'
        s.defer = true

        document.body.appendChild(s)
        let rm00 = () => document.body.removeChild(s)
        //endregion import js

        //region import css
        let l   = document.createElement('link')
        l.href  = 'https://pyscript.net/latest/pyscript.css'
        l.rel   = 'stylesheet'

        document.body.appendChild(l)
        let rm01 = () => document.body.removeChild(l)
        //endregion import css

        return () => { rm00() ; rm01() }
        //FIXME this loaded twice ref.
    }, [])

    return (
        <div>
            <py-script>print(122)</py-script>

            <hr/>
            {/* we cannot get below code working; error > (PY1000): The config supplied: [[fetch]] files = [ "./some_py_pkg/util.py", "./some_py_pkg/some_module.py", ] is an invalid TOML and cannot be parsed: SyntaxError: Expected Comment, Newline, Whitespace, or end of input but "f" found.*/}
            <py-config>
                [[fetch]]
                files = [
                    "./some_py_pkg/util.py",
                    "./some_py_pkg/some_module.py",
                ]
            </py-config>
            <py-script>
                from some_py_pkg.some_module import query
                r = query('query fr pyscript')
                print(r)
            </py-script>
        </div>
    )
}

export default TryPyscript
