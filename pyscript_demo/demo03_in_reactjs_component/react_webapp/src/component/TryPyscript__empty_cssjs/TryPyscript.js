import './pyscript_asset/pyscript.css'
import './pyscript_asset/pyscript.js'


function TryPyscript__empty_cssjs() {
  return (
      <div className='red'>We should see 122 printed in webbrowser inspector console</div>
  );
}

export default TryPyscript__empty_cssjs
