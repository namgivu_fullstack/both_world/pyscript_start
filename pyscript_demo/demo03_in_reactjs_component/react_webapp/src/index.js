import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
//
import App from './App';

import TryPyscript__empty_cssjs           from './component/TryPyscript__empty_cssjs/TryPyscript'
import TryPyscript__local_pyscript_asset  from './component/TryPyscript__local_pyscript_asset/TryPyscript'
import TryPyscript__remote_pyscript_asset from './component/TryPyscript__remote_pyscript_asset/TryPyscript'
//
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));

// root.render(<React.StrictMode>  <App />  </React.StrictMode>)
// root.render(<React.StrictMode>  <TryPyscript__empty_cssjs />           </React.StrictMode>)
// root.render(<React.StrictMode>  <TryPyscript__local_pyscript_asset />  </React.StrictMode>)
root.render(<React.StrictMode>  <TryPyscript__remote_pyscript_asset />  </React.StrictMode>)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
