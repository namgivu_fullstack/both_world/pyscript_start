from some_py_pkg.some_module import query

def onclick_query():
    # get query from elem #query_input
    e_queryinput = Element('query_input')
    q = e_queryinput.value
    r = query(q)

    # print r to elem #result
    e_result = Element('result')
    e_result.write(r)
